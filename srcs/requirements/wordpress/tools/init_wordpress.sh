#!/bin/sh

# phpmyadmin
# we create a folder in localhost
# we download the file and we move into the good folder
mkdir /var/www/localhost/phpmyadmin
wget https://files.phpmyadmin.net/phpMyAdmin/4.9.0.1/phpMyAdmin-4.9.0.1-all-languages.tar.gz
tar -xvf phpMyAdmin-4.9.0.1-all-languages.tar.gz --strip-components 1 -C /var/www/localhost/phpmyadmin
mv ./tmp/phpmyadmin.inc.php /var/www/localhost/phpmyadmin/config.inc.php

#install wordpress
# we install the wordpress into tmp
# we move into our website

cd /tmp/
wget -c https://wordpress.org/latest.tar.gz
tar -xvzf latest.tar.gz
cp -R wordpress/ ../var/www/html
mv /tmp/wp-config.php ../var/www/html/wordpress

bash