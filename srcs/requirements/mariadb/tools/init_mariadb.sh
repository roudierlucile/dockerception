#!/bin/sh

echo "CREATE DATABASE wordpress;" | mysql -u root 
echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress_boss'@'localhost' IDENTIFIED BY 'password';" | mysql -u root
echo "FLUSH PRIVILEGES;" | mysql -u root
echo "CREATE USER IF NOT EXISTS 'vagabond'@'localhost' IDENTIFIED BY 'vagabound';" | mysql -u root
service mysql restart
echo "daemon off;" >> mysql
