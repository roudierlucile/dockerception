#!/bin/sh

chown -R www-data /var/www/*
chmod -R 755 /var/www/*

mkdir ./var/www/localhost && touch ./var/www/localhost/index.php

mkdir ./etc/nginx/ssl
openssl req -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /etc/nginx/ssl/localhost.pem -keyout /etc/nginx/ssl/localhost.key -subj "/C=FR/ST=Paris/L=Paris/O=42 School/OU=lroudier/CN=localhost"

service nginx start
echo "daemon off;" >> /etc/nginx/nginx.conf
